(function (dateTimeParser) {
     
    moment.tz.setDefault("Europe/Stockholm");

    dateTimeParser.parseDeparture = function(item) {
        var parseDate = item.date +" "+item.time;
        return moment(parseDate.toString());
    }

}(window.dateTimeParser = window.dateTimeParser || {}));