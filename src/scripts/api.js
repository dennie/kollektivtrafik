(function(api){

    function parse(response){
        return $.parseJSON(JSON.stringify(response));
    }

    function getNearbyLocationIds(latidude, longitude, maxNumberOfStations, apiKey){
        var locationParameters = jQuery.param({
            key: apiKey,
            format: 'json',
            originCoordLat: latidude,
            originCoordLong: longitude
        });

        return $.ajax({
            url: 'https://api.resrobot.se/v2/location.nearbystops?' + locationParameters,
            timeout: 5000,
            dataType: 'json'
        }).then(function(response){
            if(response.error){
                return $.Deferred().reject(response.error);
            }

            var result = parse(response);

            return result.StopLocation.slice(0, maxNumberOfStations).map(location => location.id);
        }).catch(function (error) {
            console.log(error.responseText);
            return $.Deferred().reject(error);
        });
    }

    function getDepartures(id, maxJourneys, apiKey) {
        var departureParameters = jQuery.param({
            key: apiKey,
            format: 'json',
            maxJourneys: maxJourneys,
            id: id
        });

        return $.ajax({
            url: 'https://api.resrobot.se/v2/departureBoard?' + departureParameters,
            timeout: 5000,
            dataType: 'json',
        }).then(function(response){
            if(response.error){
                return $.Deferred().reject(response.error);
            }

            var result = parse(response);

            return filters.acceptedTransportationTypes(result.Departure);
        }).catch(function (error) {
            console.log(error.responseText);
            return $.Deferred().reject(error);
        });
    }

    api.getDeparturesByCoordinates = function (latitude, longitude, options){
        var settings = $.extend({
            maxNumberOfStations: 5,
            maxNumberOfJourneysPerStation: 10,
            locationApiKey: '',
            departureApiKey: ''
        }, options);

        var deferred = $.Deferred();
        var items = [];
        
        const getDeparturesByStopId = id =>  getDepartures(id, settings.maxNumberOfJourneysPerStation, settings.departureApiKey).then(departures => [ departures.map(departure => items.push(departure) ) ]);
        const getDeparturesByStopIds = list => $.when(...list.map(getDeparturesByStopId));

        getNearbyLocationIds(latitude, longitude, settings.maxNumberOfStations, settings.locationApiKey).done(function(locationIds){
            getDeparturesByStopIds(locationIds).done(function(){
                return deferred.resolve(items);
            }).fail(function (error) {
                console.log(error);
            });
        }).fail(function (error) {
            console.log(error);
        });

        return deferred.promise();
    }
}(window.api = window.api || {}));
