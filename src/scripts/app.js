$(function() {
    var latitude = '59.298440'; // Årstavägen 70
    var longitude = '18.047805';

    var departuresDisplayCount = 6;
    var cachedDepartures = [];

    var requestSettings = {
        citySettings: 'stockholm',
        maxNumberOfStations: 5,
        maxNumberOfJourneysPerStation: 20,
        locationApiKey: '0c7acd12-7f1f-4f0c-9b32-109c5074f313',
        departureApiKey: '269b9e6d-c516-4c02-bec3-aac13ed8a59e'
    }

    var colorSettings = {};
    settings.getSetting(requestSettings.citySettings).done(function (cityColorSettings) {
        colorSettings = cityColorSettings;

        recurringDepartures();
    }).fail(function (error) {
        console.log("Traffic API settings.getSetting is generating error: " + error);
    });

    var recurringDepartures = function(){
        if(cachedDepartures.length < departuresDisplayCount){
            api.getDeparturesByCoordinates(latitude, longitude, requestSettings).done(function(departures){
                sorters.sortByTime(departures);
                cachedDepartures = departures;

                recurringDepartures();
            });
        }

        cachedDepartures = filters.removeExpiredDepartures(cachedDepartures);

        $('#departures').empty();
        $.each(cachedDepartures.slice(0, departuresDisplayCount), function (idx, item) {
            $('#departures').append("<li><span class='number' style='background-color:" + formatting.getColor(item, colorSettings) + "'>" + formatting.getSymbol(item) + "</span><span class='info'><span class='direction'>" + formatting.getFormattedDirection(item) + "</span><span class='stop'>" + formatting.getFormattedStop(item) + "</span></span><span class='time'>" + formatting.getFormattedTime(item) + "</span></li>");
        });

        setTimeout(recurringDepartures, (1000 * 60));
    }
    
    recurringDepartures();

}(jQuery));