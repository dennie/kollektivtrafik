$(function () {
    $('#toggle-color-scheme').click(function () {
        $("#color-scheme-container").toggle();
    });

    $.getJSON("scripts/settings/color-settings.json")
        .then(function (settings) {
            $.each(settings.cities, function (idx, item) {
                var h2 = document.createElement("h2");
                var ul = document.createElement("ul");

                h2.innerText = item.name + " färger";

                var types = ["B", "T", "J", "S"];
                var index = 0;
                $.each(item.settings, function (idx, subItem) {
                    $(ul).append("<li><span class='number' style='background:" + subItem.color + "'>" + types[index] + " 123</span></li>");
                    if (subItem.specifics) {
                        var specificsUl = document.createElement("ul");
                        $.each(subItem.specifics, function (idx, specific) {
                            $(specificsUl).append("<li><span class='number' style='background:" + specific.color + "'>" + types[index] + " " + idx + "</span></li>");
                        });

                        $(ul).append(specificsUl);
                    }

                    index++;
                });


                $('#colors').append(h2);
                $('#colors').append(ul);
            });

        });
}());