(function(sorters){
     sorters.sortByTime = function (departures) {
        departures.sort(function (a, b) {
            return dateTimeParser.parseDeparture(a) - dateTimeParser.parseDeparture(b);
        });

        return departures;
    }
}(window.sorters = window.sorters || {}));