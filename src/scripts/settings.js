(function (settings) {

    settings.getSetting = function (city) {
        var deferred = $.Deferred();

        return $.getJSON("scripts/settings/color-settings.json")
            .then(function (settings) {
                if (settings.error) {
                    console.log("Traffic API settings.getSetting " + settings.error);

                    return $.Deferred().reject(settings.error);
                }

                var selectedSetting = $.grep(settings.cities, function (item, index) {
                    return item.name === city;
                });

                if (selectedSetting === undefined) {
                    selectedSetting = $.grep(settings.cities, function (item, index) {
                        return item.name === "default";
                    });
                }

                if (selectedSetting != undefined && selectedSetting.length > 0) {
                    return deferred.resolve(selectedSetting[0].settings);
                }

                return {};
            });
    }
}(window.settings = window.settings || {}));