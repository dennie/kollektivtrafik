(function(formatting){
    
    function removeTextWithinParantheses(text) {
        return text.replace(/ *\([^)]*\) */g, "");
    } 

    formatting.getColor = function (item, colorSettings) {
        var defaultColor = "rgba(33,33,33, .5)";

        if (colorSettings === undefined){
            return defaultColor;
        }
 
        var setting = colorSettings[item.Product.catOutS];

        if (setting === undefined) {
            return defaultColor;
        }
        
        if (setting.specifics && setting.specifics[item.Product.num]) {
            return setting.specifics[item.Product.num].color;
        }
        if (setting.color) {
            return setting.color;
        }

        return defaultColor;
    }

    formatting.getSymbol = function (item) {
        if (item.Product.catOutS === 'BLT') {
            return 'B ' + item.Product.num;
        }
        else if (item.Product.catOutS === 'ULT') {
            return 'T ' + item.Product.num;
        }
        else if (item.Product.catOutS === 'SLT') {
            return 'S ' + item.Product.num;
        } else if (item.Product.catOutS === 'JLT') {
            return 'J ' + item.Product.num;
        }


        return item.Product.name.charAt(0) + " " + item.Product.num;
    }

    formatting.getFormattedTime = function (item) {
        var departureTime = dateTimeParser.parseDeparture(item);
        var minutes = departureTime.diff(moment(), 'minutes');// Math.floor((diff / 1000) / 60);

        if (minutes < 1) {
            return 'Nu';
        }
        if (minutes < 59) {
            return minutes + ' min';
        }

        return departureTime.format('HH:mm').toString();
    }

    formatting.getFormattedStop = function (item) {
        return 'Hpl: ' + removeTextWithinParantheses(item.stop);
    }

    formatting.getFormattedDirection = function (item) {
        return removeTextWithinParantheses(item.direction);
    }

}(window.formatting = window.formatting || {}));