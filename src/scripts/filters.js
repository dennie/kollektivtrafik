(function (filters) {

     filters.removeExpiredDepartures = function(departures) {
            var filtered = departures.filter(function(item) {
                return dateTimeParser.parseDeparture(item).isAfter(moment());
            });
            return filtered;
        }

    filters.acceptedTransportationTypes = function (departures) {
        var acceptedTypes = ['BLT', 'JLT', 'SLT', 'ULT'];
        var filtered = departures.filter(function (item) {
            return acceptedTypes.indexOf(item.Product.catOutS) > -1;
        });

        return filtered;
    }

}(window.filters = window.filters || {}));
