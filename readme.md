# KOLLEKTIVTRAFIK FÖR HELA SVERIGE MED INSTÄLLNINGAR FÖR LINJEFÄRGER

![avgångar](/readme-files/departures-example.png)

Exemplet: Visar 6 stycken av de 10 närmaste avgångarna inom de 5 närmaste hållplatserna för latitude: '59.298440', longitude: '18.047805' (Årstavägen 70) och uppdateras en gång per minut

### INSTÄLLNINGAR

* Latitude och Longitude
* Antal närliggande hållplatser
* Antal avgångar per hållplats
* Antal avgångar att visa
* Inställningar för linjefärger

### KRÄVER API NYCKLAR FRÅN [SAMTRANS](https://www.trafiklab.se/)

   

    // Årstavägen 70   
    var latitude = '59.298440'; 
    var longitude = '18.047805';
    
    var requestSettings = {
    	citySettings: 'stockholm', // cities.name i json-filen
    	maxNumberOfStations: 5, // hämtar avgångar från de 5 närmaste hållplatserna
    	maxNumberOfJourneysPerStation: 20, // 20 avgångar för varje hållplats
    	locationApiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxx', //https://www.trafiklab.se/api/resrobot-reseplanerare/
    	departureApiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxx' //https://www.trafiklab.se/api/resrobot-stolptidtabeller-2/
    }
    
    api.getDeparturesByCoordinates = function (latitude, longitude, options);
      

### EXEMPEL SOM UPPDATERAR EN UL MED ID #departures EN GÅNG PER MINUT MEN, CACHAR RESPONSEN EFTERSOM [TRAFICLAB](https://www.trafiklab.se) HAR GRÄNSER FÖR ANTAL REQUEST, ETT REQUEST PER TREDJE MINUT.


    $(function() {
    	var latitude = '59.298440'; // Årstavägen 70
    	var longitude = '18.047805';
    
    	var departuresDisplayCount = 6;
    	var cachedDepartures = [];
    
    	var requestSettings = {
    		citySettings: 'stockholm',
    		maxNumberOfStations: 5,
    		maxNumberOfJourneysPerStation: 20,
    		locationApiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxx',
    		departureApiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxx'
    	}
    
    	var colorSettings = {};
    	settings.getSetting(requestSettings.citySettings).done(function (cityColorSettings) {
    		colorSettings = cityColorSettings;
    
    		recurringDepartures();
    	}).fail(function (error) {
    		console.log("Traffic API settings.getSetting is generating error: " + error);
    	});
    
    	var recurringDepartures = function(){
    		if(cachedDepartures.length < departuresDisplayCount){
    			api.getDeparturesByCoordinates(latitude, longitude, requestSettings).done(function(departures){
    				sorters.sortByTime(departures);
    				cachedDepartures = departures;
    
    				recurringDepartures();
    			});
    		}
    
    		cachedDepartures = filters.removeExpiredDepartures(cachedDepartures);
    
    		$('#departures').empty();
    		$.each(cachedDepartures.slice(0, departuresDisplayCount), function (idx, item) {
    			$('#departures').append("<li><span class='number' style='background-color:" + formatting.getColor(item, colorSettings) + "'>" + formatting.getSymbol(item) + "</span><span class='info'><span class='direction'>" + formatting.getFormattedDirection(item) + "</span><span class='stop'>" + formatting.getFormattedStop(item) + "</span></span><span class='time'>" + formatting.getFormattedTime(item) + "</span></li>");
    		});
    
    		setTimeout(recurringDepartures, (1000 * 60));
    	}
    	
    	recurringDepartures();
    
    }(jQuery));


### EXEMPEL PÅ FÄRGINSTÄLLNINGAR FÖR KOLLEKTIVTRAFIK  

Nedan färger hämtas från json-inställnings-filen, så gör man ändringar i den så kan man få en förhandsgranskning här. (De som är angivna nedan kommer från riktiga linjekartor 2018, så de kan ändras/ha ändrats)

![avgångar](/readme-files/color-scheme-example.png)

### JSON FÖR FÄRGERNA OVAN

```javascript
{
    "cities": [
        {
            "name": "default",
            "settings": {
                "BLT": {
                    "color": "rgba(33,33,33, .5)"
                },
                "ULT": {
                    "color": "rgba(33,33,33, .5)"
                },
                "JLT": {
                    "color": "rgba(33,33,33, .5)"
                },
                "SLT": {
                    "color": "rgba(33,33,33, .5)"
                }
            }
        },
        {
            "name" : "stockholm",
            "settings": {
                "BLT": {
                    "color": "rgba(143, 4, 20, .5)",
                    "specifics": {
                        "1": { "color": "rgba(0, 62, 154, .5)" },
                        "2": { "color": "rgba(0, 62, 154, .5)" },
                        "3": { "color": "rgba(0, 62, 154, .5)" },
                        "4": { "color": "rgba(0, 62, 154, .5)" }
                    }
                },
                "ULT": {
                    "color": "rgba(33,33,33, .5)",
                    "specifics": {
                        "10": { "color": "rgba(5,134,204, .5)" },
                        "11": { "color": "rgba(5,134,204, .5)" },
                        "13": { "color": "rgba(224,27,34, .5)" },
                        "14": { "color": "rgba(224,27,34, .5)" },
                        "17": { "color": "rgba(31,169,79, .5)" },
                        "18": { "color": "rgba(31,169,79, .5)" },
                        "19": { "color": "rgba(31,169,79, .5)" }
                    }
                },
                "JLT": {
                    "color": "rgba(33,33,33, .5)",
                    "specifics": {
                        "35": { "color": "rgba(239,91,157, .5)" },
                        "36": { "color": "rgba(239,91,157, .5)" },
                        "37": { "color": "rgba(239,91,157, .5)" },
                        "38": { "color": "rgba(239,91,157, .5)" },
                        "27": { "color": "rgba(170,115,175, .5)" },
                        "28": { "color": "rgba(170,115,175, .5)" },
                        "29": { "color": "rgba(170,115,175, .5)" }
                    }
                },
                "SLT": {
                    "color": "rgba(33,33,33, .5)",
                    "specifics": {
                        "7": { "color": "rgba(117,122,115, .5)" },
                        "12": { "color": "rgba(239,91,157, .5)" },
                        "21": { "color": "rgba(172,91,43, .5)" },
                        "22": { "color": "rgba(220,118,30, .5)" }
                    }
                }
            }
        }
    ]
}
```

[Öppna filen](https://bitbucket.org/dennie/kollektivtrafik/raw/6b68dfcb38730a65c59f5224a934b68ebbf847fa/src/scripts/settings/color-settings.json)

